import { useEffect, useState } from 'react';
import FingerprintJS from '@fingerprintjs/fingerprintjs-pro'
import './App.css';

function App() {
  const [fingerprintId, setFingerprintId] = useState()

  useEffect(() => {
    const getFingerprint = async () => {
      const fpPromise = FingerprintJS.load({
        token: 'VbnOowbIPx7bkWGHaS4E',
        region: 'ap',
      });
      const fp = await fpPromise
      const result = await fp.get()
      console.log(result.visitorId)
      setFingerprintId(result.visitorId);
    }
    getFingerprint()
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Fingerprint ID: {fingerprintId}
        </p>
      </header>
    </div>
  );
}

export default App;
